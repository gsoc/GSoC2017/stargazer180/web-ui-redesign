var player;
var app;
var fileInput;
var jsonData;
var data;

$(function() {  // Handler for .ready() called.
        plyrInit();
        vueInit();
        playlistInit();
        $(document).click(function(e){
            var container = $("#playlistNav");

            if($(window).width() <= 480 && !container.is(e.target) && container.has(e.target).length === 0 && container.css("width") != "0px"){
                closePlaylist();
            }
        });
});

function openNav() {
    if(screen.width <= 480){
        document.getElementById("sideNav").style.width = "60%";
    }
    else
    {
        document.getElementById("sideNav").style.width = "20%";
    }
}

function openNavMobile() {
    document.getElementById("sideNav").style.width = "60%";
}

function openPlaylist() {
    document.getElementById("playlistNav").style.width = "60%";
    document.getElementById("playlistNavMobile").style.width = "0%";
}

function closePlaylist() {
    document.getElementById("playlistNav").style.width = "0%";
    document.getElementById("playlistNavMobile").style.width = "10%";
}

function closeNav() {
    document.getElementById("sideNav").style.width = "0";
}   

function playlistInit()
{
    sendCommand(1);
}

function populatePlaylist()
{
    data = JSON.parse(jsonData);
    for (var i = 0; i < data.children[0].children.length; i++) {
        app.addItem(data.children[0].children[i].id, data.children[0].children[i].name, data.children[0].children[i].src);
    }
}

function toggleRepeat()
{
    sendCommand(0,"command=pl_repeat");
}

function removeCurrent()
{
    sendCommand(0,"command=pl_delete&id="+data.children[0].children[0].id);
    app.removeItem(data.children[0].children[0].id);
    playlistInit();
}

function sendCommand(mode, params) {
    if(mode == 0)
    {
        $.ajax({
            url: 'requests/status.json',
            data: params,
            success: function (data, status, jqXHR) {
                jsonData = data;
            }
        });
    } else if(mode == 1)
    {
        $.ajax({
            url: 'requests/playlist.json',
            data: params,
            success: function (data, status, jqXHR) {
                jsonData = data;
                populatePlaylist();
            }
        });
    } else if(mode == 2)
    {

    } else if(mode == 3)
    {
        
    } else if(mode == 4)
    {
        
    } else if(mode == 5)
    {
        
    }
}