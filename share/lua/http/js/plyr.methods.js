function plyrInit()
{
    player = plyr.setup({debug:false,showPosterOnEnd:true});
    setVideo('S6IP_6HG2QE','youtube');
    player[0].on('pause', function() {
        sendCommand(0,"command=pl_pause");     
    });

    player[0].on('play', function() {
        sendCommand(0,"command=pl_pause");
    });

    player[0].on('volumechange', function(event) {
        var cmd = "command=volume&val="+event.detail.plyr.getVolume()*255;
        sendCommand(0,cmd);
    });
    
    player[0].on('loadstart', function(event) {
        
        sendCommand(0,"command=pl_play&id="+id);
    });
}

function setVideo(videoSrc, type, id)
{
    
    player[0].source({
        type: 'video',
        title: '',
        sources: [{
        src: videoSrc,
        type: type
        }],
        poster: "assets/vlc.png"
    });
}