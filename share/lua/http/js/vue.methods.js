function vueInit()
{
    /*Vue.component('app', {
        template: '#app-template',
        data: function () {
            return {
                msg: 'Welcome to Vue.js World!'
            }
        }
    });*/

    app = new Vue({
        el: '#app',
        data: {
            playlist: []
        },
        methods: {
            play: function (msg, id) {
                setVideo(msg,'video/mp4', id);
                player[0].play();
            },
            addItem: function(id, title, src)
            {
                //console.log(playlist);
                this.playlist.push({ id: id, title: title, src: src });
            },
            removeItem: function(id)
            {
                this.playlist.splice({ id: id });
            }
        }
    });
}